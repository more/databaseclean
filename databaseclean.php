#!/usr/bin/php
<?php
date_default_timezone_set('Europe/Helsinki');

$db_host = "localhost";
$default_batch_size=1000;

array_shift($argv);
$db_username = $argv[0];
$db_password = $argv[1];
$db_database = $argv[2];
$db_table = $argv[3];
$db_column = $argv[4];
$delete_days = $argv[5];
$batch_size = $argv[6];

function logMessage($message) {
  echo date('Y-m-d H:i:s') . " - " . $message . "\n";
}

$msg = "";
if (strlen($db_username) == 0) {
  $msg .= " Missing DB_USERNAME\n";
}
if (!isset($db_password)) {
  $msg .= " Missing DB_PASSWORD\n";
}
if (!isset($db_database)) {
  $msg .= " Missing DB_DATABASE_NAME\n";
}
if (!isset($db_table)) {
  $msg .= " Missing DB_TABLE_NAME\n";
}
if (!isset($db_column)) {
  $msg .= " Missing DB_COLUMN_NAME\n";
}
if (!isset($delete_days)) {
  $msg .= " Missing REMOVE_DAYS\n";
}
if ($delete_days <= 0) {
  $msg .=  " REMOVE_DAYS not positive";
}
if (!isset($batch_size) || $batch_size <= 0) {
  $batch_size = $default_batch_size;
}

if (strlen($msg) > 0) {
  logMessage("Error executing database clean:\n$msg\nUsage: php database-clean DB_USERNAME DB_PASSWORD DB_DATABASE_NAME DB_TABLE_NAME DB_COLUMN_NAME REMOVE_DAYS [BATCH_SIZE]\n");
  exit();
}

logMessage("Connecting to $db_host.$db_database as $db_username");
$mysqli = new mysqli($db_host, $db_username, $db_password, $db_database);

if (mysqli_connect_errno()) {
  logMessage("DB connect failed: " . mysqli_connect_error());
  exit();
}

logMessage("Deleting data older than $delete_days days from $db_table using $db_column in batches of $batch_size");
$total_deleted_rows = 0;
$affected_rows = 0;
$sql = "DELETE FROM $db_table WHERE $db_column < DATE(DATE_SUB(CURDATE(), INTERVAL $delete_days DAY)) LIMIT $batch_size;";
logMessage($sql);
do {
  $mysqli->query($sql);
  $affected_rows = $mysqli->affected_rows;
  if ($affected_rows < 0) {
    logMessage($mysqli->error);
  }
  $total_deleted_rows += $affected_rows;
} while ($affected_rows > 0);
logMessage("Deleted rows: " . $total_deleted_rows);

$mysqli->close();
?>
